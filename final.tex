ss{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{subfigure}

\title{Music Classification: A Topological Approach}
\author{Alex Calderwood \\
James Corbett \\
Angus Tomlinson}
\date{April 2016}

\begin{document}

\maketitle
\section{Abstract}

Persistent Homology is used to analyze topological similarities and trends in data for many applications. In this paper we use two algorithms, SIF and SIFTS, to analyze the persistent homology of music by creating bar graphs and persistence diagrams for individual songs. Our work is based off the paper "Persistent Homology: An Introduction and a New Text Representation for Natural Language Processing" by Xiaojin Zhu. 

Currently, comparisons between songs are performed at the cursory level but through this paper we hope to lay the foundation for more rigorous evaluation in future research. Our hope is that by applying a distance function over the homology of thousands of songs we would be able quantitatively compare and contrast music. These comparisons could become an invaluable resource for tools such as the Music Genome Project, the program used by Pandora to give each user music selection catered specifically for them.

\section{Motivation and First Concepts}

Music analysis has been a popular motivator for improving computational techniques and algorithms since the beginning of the Internet. Napster, the first popularized peer-to-peer file sharing program, was created for the purpose of efficiently finding and sharing popular music. Now there are hundreds of programs used daily to find and share music, the most popular being Spotify and Pandora. Both programs pride themselves on their ability to cater their content to individual users, and it is one of the core features that makes each program so popular. 

We speculate that applications of analyzing music using Topological Data Analysis (TDA) include the ability to drastically increase the rate at which algorithms may correctly match a song to a user's taste, mood, and history. Our goal is to help advance musical analysis through the use of persistent homology techniques which should be able identify quality songs for example, or otherwise not conforming to traditional classification measures.
\section{SIF and SIFTS}

Xiaojin Zhu's paper introduced two algorithmic approaches to analyzing textual data, SIF and SIFTS. Since we are trying to be more topical with this paper, we will not produce the algorithm here, but will explain the idea. The concept behind the first algorithm, SIF, is simple. It requires that the user of the algorithm first identifies a textual unit with some length, such as sentences in a short-story, lines of poetry in a poem, or paragraphs in a novel. Once this is done, each unit is treated as a frequency vector or 'bag of words', i.e. the frequency of each word in the sentence is computed and added as a component in the vector. What this means in practice is that each possible word must be a distinct axis in the space of all possible frequency vectors $F^V$, where V is the size of the vocabulary. Since a given genre's vocabulary may be on the order of magnitude of 10's of thousands of words, the complexity and distances between vectors becomes very large for any interesting domain.

Once these vectors are computed, the SIF algorithm proceeds by computing Vietoris-Rips on the vectors\----varying the $\epsilon$ value, defined as the minimum distance between vectors (vertices or $p_0$ chains in the simplicial complex) to which we add a connection, (an edge or $p_1$ chain). Finally, homology is calculated, the varying births and deaths recorded, and these are plotted as persistence diagrams and barcodes.

The next algorithm, SIFTS, is almost identical to SIF but with one crucial difference. That is, before the Vietoris-Rips complex is computed, a 'time-skeleton' is added to the complex. This is a series of p-1 chains that represent consecutive text units. When sentence B follows sentence A in a text, an edge\----an element of the time skeleton\----is added to represent this adjacency. What this results in is a $0^{th}$ order homology of always exactly one connected component, since every text unit is connected to the unit before and after it, with the exception of the first and last. However, adding this time skeleton also results in possibly the most interesting aspect of the entire TDA approach: the loops discovered when computing the $1^{st}$ order homology often represent loops in which there has been some degree of repetition, in other words a return to an already visited part of the space F. In text, this is a repetition of frequency in words, and this repetition may represent tie-backs in the semantics of the text.

\section{Music}

Originally our idea was to apply the theoretical results of this paper to musical lyrics, before we made the realization that we might also be able to apply the algorithms presented in the paper to musical notes themselves. We considered comparing note pitches to lyrics, in order to determine the similarities in the 'shape' of a song's lyrics with its notes, but that idea took a backseat to simply comparing a song's structure with that of other songs. SIFTS here works just as it does with words. However, rather than a sentence, you have a measure, and rather than a 'bag of words' this frequency vector is a 'bag of notes'. Semantic tie-backs remain, but repetitions become loosely structured patterns in musical compositions. We speculated that the shapes produced by these algorithms when applied to music would result in topological similarities not found with traditional classification techniques. 

We built upon the code written by Zhu and his collaborators to apply SIF and SIFTS to music files, specifically files of the 'MusicXML' format. We developed software that parses MusicXML, extracting the pitches and then creating vectors that represent bag of notes vectors. Our software is slightly more robust than Zhu's, and creates persistence diagrams in addition to barcodes.

\section{Results: Observed Similarities and Differences}

\begin{figure}[H]
    \centering
        \includegraphics[scale=0.5]{radioactive0_sif_barcode.jpg}
	    \caption{Radioactive by Imagine Dragons - SIF Barcodes}
	        \label{fig:midi}
		\end{figure}

		\begin{figure}[H]
		    \centering
		        \includegraphics[scale=0.5]{radioactive0_sif_persistence.jpg}
			    \caption{Radioactive by Imagine Dragons - SIF Persistence Diagrams}
			        \label{fig:midi}
				\end{figure}

				\begin{figure}[H]
				    \centering
				        \includegraphics[scale=0.5]{radioactive0_sifts_barcode.jpg}
					    \caption{Radioactive by Imagine Dragons - SIFTS Barcodes}
					        \label{fig:midi}
						\end{figure}

						\begin{figure}[H]
						    \centering
						        \includegraphics[scale=0.5]{radioactive0_sifts_persistence.jpg}
							    \caption{Radioactive by Imagine Dragons - SIFTS Persistence Diagrams}
							        \label{fig:midi}
								\end{figure}

								In order to visualize the results of our research, we chose to use barcodes and persistence diagrams, which could provide deeper insights into the structure of the multidimensional note complexes. To produce the $0^{th}$ and $1^{st}$ order homology barcodes, we employed code written by Zhu and his colleagues. In addition to the barcodes, we wrote our own code to compute the song persistence diagrams.

								Because music is intrinsically structured, we expected to find distinctive patterns in the notes of songs. With this intuition, we decided to sample songs from two widely-different genres and examine if there were any distinctive structure differences between the two. For the genres, we chose modern pop and classical. To represent the pop genre, we selected from a wide variety of modern pop artists such as Bastille, Imagine Dragons, One Republic, and X-Ambassadors; for classical music, we chose from a number of Vivaldi's compositions. 

								As we began to examine the barcodes and persistence diagrams produced by the songs, we began to notice some very interesting features. Many modern pop songs like 'Counting Stars', 'Radioactive', and 'Renegades' have a similar $H_1$ SIFTS barcode profile that differs drastically from Vivaldi's pieces. Also of interest, the songs of artists like Imagine Dragons and One Republic had similar persistence characteristics in their songs. These persistence features can be observed in the diagrams of 'Radioactive' above, and in the many song figures in the appendix.

								Even though we only briefly tapped into the incredible world of music data, we immediately saw clear persistence similarities within the musical works of artists and genres. Though our current method of comparing persistent homologies is purely subjective, we believe that we have at least completed the first and most important part of the project, having verified that similarities seem to exist in places you'd expect them. We see that different instrumental parts of the same song appear to be similar to other parts of the same song, just as we see similar clusters in persistence diagrams of songs of the same artist and genre. Similarly, some songs have clear differences. The persistence diagram for the song 'Pompeii' for example, contains a cluster whose range is almost entirely disjoint from the clusters in Pirates of the Caribbean's 'He's a Pirate.'

								\section{Future Work}
								In the future, we hope to implement mathematically sound functions to analyze the persistence diagrams. A specific tool studied, explained in [REF], uses two persistence diagrams as input to produce a "distance" between the two graphs. This is accomplished by reducing the two persistence diagrams to an unmatched bipartite graph and finding the bottleneck or Wasserstein weight [REF] of that graph. The two point sets for the bipartite graph are generated by taking the union between two sets of points generated from the persistence diagrams. These points are the off diagonal points of one graph, and the orthogonal projections to the diagonal of the off diagonal points from the other graph, for both combinations. This metric serves as a quantitative measure of how similar or different two data sets, in our case the persistence diagrams of musical compositions, are to each other. By calculating this distance over many songs we hope to show topologically consistent similarities between genres, specific artists, or even periods of time in music history. The library to implement this algorithm has been made publicly available, however do to some build issues and time constraints, was not included in this paper.

								\section{Conclusion}
								Our most basic goal, when creating persistence diagrams and bar graphs using songs, was to find meaningful topological similarities and differences in music. Currently, our only method for comparing persistence diagrams is through inspecting the graphs visually and noting obvious trends. This method can be used, with some success, to compare songs which are either extremely similar or different in structure. This method of analysis is inherently inefficient and did not allow us to gather conclusive results for our data. In this paper we reference a tool for comparing persistence diagrams by reducing the diagrams to bipartite graphs who's weights, or distances, can be calculated. We believe that by creating a large enough data set of distances, we would be able to determine mathematically how similar the structure of different pieces of music actually are.
								\section{References}

								Kerber, Michael, Dmitriy Morozov, and Arnur Nigmetov. Geometry Helps to Compare Persistence Diagrams. Rep. Online. 

								Web. 29 Apr. 2016. <https://www.pandora.com/about/mgp>.

								"Peer-to-peer File Sharing." Wikipedia. Wikimedia Foundation, 18 Feb. 2014. Web. 01 May 2016.

								\section{Appendix}

								\begin{figure}[H]
								    \centering
								        \includegraphics[scale=0.5]{apologize0_sif_barcode.jpg}
									    \caption{Apologize by One Republic - SIF Barcodes}
									        \label{fig:midi}
										\end{figure}

										\begin{figure}[H]
										    \centering
										        \includegraphics[scale=0.5]{apologize0_sif_persistence.jpg}
											    \caption{Apologize by One Republic - SIF Persistence}
											        \label{fig:midi}
												\end{figure}

												\begin{figure}[H]
												    \centering
												        \includegraphics[scale=0.5]{apologize0_sifts_barcode.jpg}
													    \caption{Apologize by One Republic - SIFTS Barcodes}
													        \label{fig:midi}
														\end{figure}

														\begin{figure}[H]
														    \centering
														        \includegraphics[scale=0.5]{apologize0_sifts_persistence.jpg}
															    \caption{Apologize by One Republic - SIFTS Persistence}
															        \label{fig:midi}
																\end{figure}

																\begin{figure}[H]
																    \centering
																        \includegraphics[scale=0.5]{vivaldi_rain1_sif_barcode.jpg}
																	    \caption{Spring by Vivaldi - SIF Barcodes}
																	        \label{fig:midi}
																		\end{figure}

																		\begin{figure}[H]
																		    \centering
																		        \includegraphics[scale=0.5]{vivaldi_rain1_sif_persistence.jpg}
																			    \caption{Spring by Vivaldi - SIF Persistence}
																			        \label{fig:midi}
																				\end{figure}

																				\begin{figure}[H]
																				    \centering
																				        \includegraphics[scale=0.5]{vivaldi_rain1_sifts_barcode.jpg}
																					    \caption{Spring by Vivaldi - SIFTS Barcodes}
																					        \label{fig:midi}
																						\end{figure}

																						\begin{figure}[H]
																						    \centering
																						        \includegraphics[scale=0.5]{vivaldi_rain1_sifts_persistence.jpg}
																							    \caption{Spring by Vivaldi - SIFTS Persistence}
																							        \label{fig:midi}
																								\end{figure}



																								\end{document}

